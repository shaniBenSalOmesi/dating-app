using System.Linq;
using AutoMapper;
using DatingApp.API.Data.Dto;
using DatingApp.API.Models;

namespace DatingApp.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User,UserForListDto>()
            .ForMember(
                dest => dest.PhotoUrl, 
                opt => opt.MapFrom(src => src.Photos.FirstOrDefault(p=> p.IsMain).Url))
                .ForMember(
                    dest => dest.Age,
                    opt => opt.MapFrom(src => src.DateOfBirth.CalculateAge())
                );

            CreateMap<User,UserForDetailsDto>()
            .ForMember(
                dest => dest.PhotoUrl, 
                opt => opt.MapFrom(src => src.Photos.FirstOrDefault(p=> p.IsMain).Url))
                .ForMember(
                    dest => dest.Age,
                    opt => opt.MapFrom(src => src.DateOfBirth.CalculateAge())
                );   
                //since there is no "calculated data" the mapp stays in its basic form.
            CreateMap<Photo, PhotosForDetailiedDto>();
            CreateMap<UpdateUserDto, User>();
            CreateMap<Photo, PhotosToReturnDto>();
            CreateMap<AddedPhotoDto, Photo>();
            CreateMap<UserForRegistrationDto, User>();
            CreateMap<MessageForCreationDto, Message>().ReverseMap();
            CreateMap<Message, MessageToReturnDto>()
            .ForMember(m => m.SenderPhotoUrl, opt => opt.MapFrom(u=> u.Sender.Photos.FirstOrDefault(p=> p.IsMain).Url))
            .ForMember(m => m.RecipientPhotoUrl, opt => opt.MapFrom(u=> u.Recipient.Photos.FirstOrDefault(p=> p.IsMain).Url));

        }
    }
}