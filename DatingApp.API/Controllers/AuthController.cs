using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Data;
using DatingApp.API.Data.Dto;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IAuthRepository _repo;
        private readonly IMapper _mapper;
        public AuthController(IAuthRepository repo,
        IMapper mapper, //Lesson 117(1)-> use in login method
        IConfiguration config) // 6 import the configuration
        {
            _mapper = mapper;
            _config = config;
            _repo = repo;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegistrationDto userForRegister)
        {
            //we will want to save the name as lower case
            userForRegister.UserName = userForRegister.UserName.ToLower();
            //check if user exsist
            if (await _repo.UserExist(userForRegister.UserName))
            {
                return BadRequest("username already exist");
            };
            //create new entity before creating mapper
            // var newUser = new User { UserName = userForRegister.UserName };

            var newUser = _mapper.Map<User>(userForRegister);
            //we need to genrate the hash salt password using the new entity
            var userPassword = await _repo.Register(newUser, userForRegister.Password);
            var userToReturn = _mapper.Map<UserForDetailsDto>(userPassword);
            // since we not yet genrate a true user we will return succses code;
            return CreatedAtRoute("GetUser", new {Controller="Users", id = userPassword.Id}, userToReturn);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userForLogin)
        {
            //1 
            var user = await _repo.Login(userForLogin.UserName.ToLower(), userForLogin.Password);
            if (user == null) return Unauthorized();
            //2 build the token contain the user Id and the user name.
            //3 this is the header and the body of JWT
            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName)
            };
            //4 creating the encript token hashed part we will want to save it in app settings.
            //5 we will need the configuration in order to save the token for later request
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            //7 generate the credentials
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            //8 create the token body including te expired date
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            //Lesson 117(2)-> we want the user photo so after the login we also sent the user data
            var userToReturn = _mapper.Map<UserForListDto>(user);
            return Ok(new
            {
                token = tokenHandler.WriteToken(token),
                userToReturn
            });
        }

    }
}