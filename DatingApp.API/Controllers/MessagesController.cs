using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DatingApp.API.Data.Dto;
using DatingApp.API.Data.IDating;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DatingApp.API.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Authorize]
    [Route("api/users/{userId}/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IDatingRepository _repo;
        private readonly IMapper _mapper;
        public MessageController(IDatingRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet("{id}", Name = "GetMessage")]
        public async Task<IActionResult> GetMessage(int userId, int id)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) return Unauthorized();
            var messageFromRepo = await _repo.GetMessage(id);
            if (messageFromRepo == null) return NotFound();
            return Ok(messageFromRepo);
        }

        [HttpPost]
        public async Task<IActionResult> CreateMessage(int userId, MessageForCreationDto msg)
        {
            var sender = await _repo.GetUser(userId);
            if (sender.Id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) return Unauthorized();
            msg.SenderId = userId;
            var recipient = await _repo.GetUser(msg.RecipientId);
            if (recipient == null) return NotFound();
            var massage = _mapper.Map<Message>(msg);
            _repo.Add(massage);
            if (await _repo.SaveAll())
            {
                var msgToReturn = _mapper.Map<MessageToReturnDto>(massage);
                return CreatedAtRoute("GetMessage", new { userId = userId, id = massage.Id }, msgToReturn);
            }
            throw new Exception("Creating new message faild!");
        }

        [HttpGet]
        public async Task<IActionResult> GetMessageForUser(int userId, [FromQuery]MessageParams msgParams)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) return Unauthorized();
            msgParams.UserId = userId;
            var msgList = await _repo.GetMessagesForUser(msgParams);
            var msg = _mapper.Map<IEnumerable<MessageToReturnDto>>(msgList);
            Response.AddPagination(msgList.CurrentPage, msgList.PageSize, msgList.TotalCount, msgList.TotalPages);
            return Ok(msg);
        }

        [HttpGet("thread/{recepientId}")]
        public async Task<IActionResult> GetMessageThread(int userId, int recepientId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) return Unauthorized();
            var messageFromRepo = await _repo.GetMessageThread(userId, recepientId);
            var msgToReturn = _mapper.Map<IEnumerable<MessageToReturnDto>>(messageFromRepo);
            if (msgToReturn == null) return NotFound();
            return Ok(msgToReturn);
        }

        [HttpPost("{messageId}")]
        public async Task<IActionResult> DeleteMessage(int userId, int messageId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) return Unauthorized();
            var messageFromRepo = await _repo.GetMessage(messageId);
            if (messageFromRepo.SenderId == userId)
            {
                messageFromRepo.SenderDeleted = true;
            }
            if (messageFromRepo.RecipientId == userId)
            {
                messageFromRepo.RecipientDeleted = true;
            }
            if (messageFromRepo.RecipientDeleted && messageFromRepo.SenderDeleted)
            {
                _repo.Delete(messageFromRepo);
            }
            if (await _repo.SaveAll())
            {
                return NoContent();
            }
            throw new Exception("Error with delete controller!");
        }

        [HttpPost("{messageId}/read")]
        public async Task<IActionResult> MessageRead(int userId, int messageId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value)) return Unauthorized();
            var messageFromRepo = await _repo.GetMessage(messageId);
            if(messageFromRepo.RecipientId != userId) return Unauthorized();
            messageFromRepo.IsRead = true;
            messageFromRepo.DateRead = DateTime.Now;
            if (await _repo.SaveAll())
            {
                return NoContent();
            }
            throw new Exception("Error with set read option!");


        }
    }
}