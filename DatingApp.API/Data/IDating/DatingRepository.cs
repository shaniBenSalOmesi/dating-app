using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatingApp.API.Helpers;
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data.IDating
{
    public class DatingRepository : IDatingRepository
    {
        private readonly DataContext _context;
        public DatingRepository(DataContext context)
        {
            _context = context;
        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }
        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.Include(p => p.Photos).FirstOrDefaultAsync(u => u.Id == id);
            return user;
        }
        public async Task<PageList<User>> GetUsers(UserParams userParams)
        {
            //before we return the users as pageList
            // var users = await _context.Users.Include(p=> p.Photos).ToListAsync();
            // return users;

            var users = _context.Users.Include(p => p.Photos).OrderByDescending(u => u.LastActive).AsQueryable();

            users = users.Where(u => u.Id != userParams.UserId);
            users = users.Where(u => u.Gender == userParams.Gender);
            if (userParams.Likers)
            {
                var userLikers = await GetUserLikes(userParams.UserId, userParams.Likers);
                users = users.Where(u => userLikers.Contains(u.Id));
            }
            if (userParams.Likees)
            {
                var userLikees = await GetUserLikes(userParams.UserId, userParams.Likers);
                users = users.Where(u => userLikees.Contains(u.Id));
            }
            if (userParams.MinAge != 18 || userParams.MaxAge != 99)
            {
                var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                var maxDob = DateTime.Today.AddYears(-userParams.MinAge);
                users = users.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);
            }

            if (!string.IsNullOrEmpty(userParams.OrderBy))
            {
                switch (userParams.OrderBy)
                {
                    case "created":
                        users = users.OrderByDescending(users => users.Created);
                        break;
                    default:
                        users = users.OrderByDescending(users => users.LastActive);
                        break;
                }
            }
            return await PageList<User>.CreateAsync(users, userParams.PageNumber, userParams.PageSize);

        }

        public async Task<IEnumerable<int>> GetUserLikes(int userId, bool likers)
        {
            var user = await _context.Users.Include(x => x.Liker).Include(x => x.Likees).FirstOrDefaultAsync(u => u.Id == userId);
            if (likers)
            {
                return user.Liker.Where(u => u.LikeeId == userId).Select(i => i.LikerId);
            }
            else
            {
                return user.Likees.Where(u => u.LikerId == userId).Select(i => i.LikeeId);
            }
        }
        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<Photo> GetPhoto(int id)
        {
            var photo = await _context.Photos.FirstOrDefaultAsync(p => p.Id == id);
            return photo;
        }

        public async Task<Photo> GetMainPhotoForUser(int userId)
        {
            return await _context.Photos.Where(u => u.UserId == userId).FirstOrDefaultAsync(p => p.IsMain);
        }

        public async Task<Like> GetLike(int userId, int recepientId)
        {
            return await _context.Likes.FirstOrDefaultAsync(u => u.LikerId == userId && u.LikeeId == recepientId);
        }

        public async Task<Message> GetMessage(int id)
        {
            return await _context.Messages.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<PageList<Message>> GetMessagesForUser(MessageParams param)
        {
            var msgFromContext = _context.Messages
            .Include(u => u.Sender).ThenInclude(p=>p.Photos)
            .Include(u => u.Recipient).ThenInclude(p=>p.Photos).AsQueryable();

            switch(param.MessageContainer)
            {
                case "Inbox":
                msgFromContext = msgFromContext.Where(u => u.RecipientId == param.UserId && u.RecipientDeleted == false);
                break;
                case "Outbox":
                msgFromContext = msgFromContext.Where(u => u.SenderId == param.UserId && u.SenderDeleted == false);
                break;
                default:
                msgFromContext = msgFromContext.Where(u => u.RecipientId == param.UserId && u.IsRead == false && u.RecipientDeleted == false);
                break;
            }
            msgFromContext = msgFromContext.OrderByDescending(d => d.MessageSent);
            return await PageList<Message>.CreateAsync(msgFromContext, param.PageNumber, param.PageSize);
        }

        public async Task<IEnumerable<Message>> GetMessageThread(int userId, int recepientId)
        {
            var msgFromContext = await _context.Messages
            .Include(u => u.Sender).ThenInclude(p=>p.Photos)
            .Include(u => u.Recipient).ThenInclude(p=>p.Photos)
            .Where(m => m.RecipientId == userId && m.SenderId == recepientId && m.RecipientDeleted == false ||
                        m.RecipientId == recepientId && m.SenderId == userId && m.SenderDeleted == false )
                        .OrderByDescending(m => m.MessageSent).ToListAsync();
            return msgFromContext;
        }
    }
}