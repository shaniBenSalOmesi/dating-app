using System;
using System.Threading.Tasks;
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DatingApp.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;
        public AuthRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<User> Login(string username, string password)
        {
            //first we need to convert the password to hash and then compare to what exsit in the db
            // we first get the users with the same username
            //Lesson 117-> I added the include in order to get with the user his photos.
            var user = await _context.Users.Include(p=> p.Photos).FirstOrDefaultAsync(x => x.UserName == username);
            //if no user or the hash password test fails we return null
            if (user == null) return null;
            if (!VerifiedPasswordHash(password, user.PasswordHash, user.PasswordSalt)) return null;
            return user;
        }

        private bool VerifiedPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (
            //this function is very similar to create the hash pass now reverse.
            // we send the salt
            var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt)
            )
            {
                //we convert the income password to hash
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                //we compare with for loop between the password from the db to the generated one
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i]) return false;
                }
            }
            return true;
        }

        public async Task<User> Register(User user, string password)
        {
            //decleare the variables
            byte[] passwordSalt, passwordHash;
            //generate the hash and salt password
            // out type means that it genrate a copy of the params and install the res from the function to the origin params.
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            //after we install the passwords types, we want to add the user async.
            await _context.Users.AddAsync(user);

            //save the changes
            await _context.SaveChangesAsync();
            return user;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (
            var hmac = new System.Security.Cryptography.HMACSHA512()
            )
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public async Task<bool> UserExist(string username)
        {
            return await _context.Users.AnyAsync(x => x.UserName == username);
        }
    }
}