using DatingApp.API.Controllers.Models; //refering the Value type
using DatingApp.API.Models;
using Microsoft.EntityFrameworkCore;
namespace DatingApp.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options){} //constructor

        public DbSet<Value> Values {get; set;} //create the property
        public DbSet<User> Users {get; set;} //create the property
        public DbSet<Photo> Photos {get; set;}
        public DbSet<Like> Likes { get; set; }
        public DbSet<Message> Messages {get; set;}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Like>().HasKey(k => new {k.LikeeId, k.LikerId}); // the entetis dosent have id so we create one.
            
            builder.Entity<Like>().HasOne(u => u.Likee)
            .WithMany(u => u.Liker)
            .HasForeignKey(u => u.LikeeId) //the Id to relate
            .OnDelete(DeleteBehavior.Restrict);//we dont want that delete of like will delete the user

            builder.Entity<Like>().HasOne(u => u.Liker)
            .WithMany(u => u.Likees)
            .HasForeignKey(u => u.LikerId)
            .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Message>().HasOne(u => u.Sender)
            .WithMany(m => m.MessagesSent)
            .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Message>().HasOne(u => u.Recipient)
            .WithMany(m => m.MessagesRecived)
            .OnDelete(DeleteBehavior.Restrict);
        }
    }
}