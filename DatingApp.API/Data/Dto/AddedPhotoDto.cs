using System;
using Microsoft.AspNetCore.Http;

namespace DatingApp.API.Data.Dto
{
    public class AddedPhotoDto
    {
        public string Url {get; set;}
        public IFormFile File {get; set;}
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public string PublicId { get; set; }

        public AddedPhotoDto()
        {
            DateAdded = DateTime.Now;
        }
    }
}