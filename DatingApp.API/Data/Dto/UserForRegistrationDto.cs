using System;
using System.ComponentModel.DataAnnotations;

namespace DatingApp.API.Data.Dto
{
    public class UserForRegistrationDto
    {
        //username and password cannot be blank
        [Required]
        public string UserName { get; set; }
        [Required]
        //another validation
        [StringLength(8, MinimumLength = 4, ErrorMessage="Password should be between to 4 char")]
        public string Password { get; set; }
        [Required]
        public string Gender { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string KnownAs { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public UserForRegistrationDto()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}