using System;

namespace DatingApp.API.Data.Dto
{
    public class PhotosToReturnDto
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public string PublicId { get; set; }

        public bool IsMain { get; set; }
    }
}