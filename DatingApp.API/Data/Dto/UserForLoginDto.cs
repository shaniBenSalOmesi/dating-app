
namespace DatingApp.API.Data.Dto
{
    public class UserForLoginDto
    {
        //we dont need here validation since its happend in the methods 
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}