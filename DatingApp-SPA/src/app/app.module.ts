import { BrowserModule } from '@angular/platform-browser';
import { JwtModule } from '@auth0/angular-jwt';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TimeagoModule } from 'ngx-timeago';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AuthService } from './_service/auth.service';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ErrorInterceptorProvider } from './_service/interceptor/error.interceptor';
import { BsDropdownModule, TabsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListsComponent } from './lists/lists.component';
import { MembersComponent } from './members/members.component';
import { MessagesComponent } from './messages/messages.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { LoginComponent } from './nav-bar/login/login.component';
import { UserOptionsComponent } from './nav-bar/user-options/user-options.component';
import { MemberCardComponent } from './members/member-list/member-card/member-card.component';
import General from './_service/general.const';
import { MemberDetailsComponent } from './members/member-details/member-details.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { MemberEditComponent } from './members/member-edit/member-edit.component';
import { MemberEditResolver } from './_resolvers/member-edit.resolver';
import { PreventUnsavedChangesGuard } from './_guards/prevent-unsaved-cahnges.guard';
import { PhotoUploadComponent } from './members/photo-upload/photo-upload.component';
import { FileUploadModule } from 'ng2-file-upload';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { MemberListComponent } from './members/member-list/member-list.component';
import { MembersFiltersComponent } from './members/member-list/members-filters/members-filters.component';
import { ListResolver } from './_resolvers/list.resolver';
import { MessagesListResolver } from './_resolvers/messages.resolver';
import { MemberMessagesComponent } from './members/member-messages/member-messages.component';
import { MemderDetailsLeftComponent } from './members/memder-details-left/memder-details-left.component';
import { StoreModule, ActionReducerMap } from '@ngrx/store';
import { TryLoginEffect } from './_store/effects/tryLogin.effect';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { userReducer } from './_store/reducer/user.reducer';
import { AppState } from './_store/state';
import { TryGetMembersEffect } from './_store/effects/getMemberList.effect';
import { membersReducer } from './_store/reducer/members.reducer';
import { GetSelectedMemberEffect } from './_store/effects/getSelectedMember.effect';
import { MemberDetailsWrapperComponent } from './members/member-details/member-details-wrapper/member-details-wrapper.component';
import { messageReducer } from './_store/reducer/messages.reducer';
import { TryGetMessagesEffect } from './_store/effects/messeges/getThreadMessages.effect';
import { MarkAsMsgReadEffect } from './_store/effects/messeges/markMessageRead.effect';

export function tokenGetter() {
  return localStorage.getItem(General.token);
}
export const appReducers: ActionReducerMap<AppState> = {
  userState: userReducer,
  memberState: membersReducer,
  messageState: messageReducer
};
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    RegisterComponent,
    ListsComponent,
    MembersComponent,
    LoginComponent,
    MemberListComponent,
    UserOptionsComponent,
    MessagesComponent,
    MemberDetailsComponent,
    MemberCardComponent,
    PhotoUploadComponent,
    MemberEditComponent,
    MembersFiltersComponent,
    MemberMessagesComponent,
    MemderDetailsLeftComponent,
    MemberDetailsWrapperComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgxGalleryModule,
    TimeagoModule.forRoot(),
    PaginationModule.forRoot(),
    ButtonsModule.forRoot(),
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:5000'],
        blacklistedRoutes: ['localhost:5000/api/auth']
      }
    }),
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([TryLoginEffect, TryGetMembersEffect, GetSelectedMemberEffect,
      TryGetMessagesEffect, MarkAsMsgReadEffect]),
    StoreDevtoolsModule.instrument({ maxAge: 5 })
  ],
  providers: [
    AuthService,
    ErrorInterceptorProvider,
    MemberEditResolver,
    PreventUnsavedChangesGuard,
    ListResolver,
    MessagesListResolver
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
