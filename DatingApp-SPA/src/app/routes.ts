import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MembersComponent } from './members/members.component';
import { MessagesComponent } from './messages/messages.component';
import { ListsComponent } from './lists/lists.component';
import { AuthGuard } from './_guards/auth.guard';
import { MemberDetailsComponent } from './members/member-details/member-details.component';
import { MemberEditComponent } from './members/member-edit/member-edit.component';
import { MemberEditResolver } from './_resolvers/member-edit.resolver';
import { PreventUnsavedChangesGuard } from './_guards/prevent-unsaved-cahnges.guard';
import { ListResolver } from './_resolvers/list.resolver';
import { MessagesListResolver } from './_resolvers/messages.resolver';
import { MemberDetailsWrapperComponent } from './members/member-details/member-details-wrapper/member-details-wrapper.component';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'members', component: MembersComponent, pathMatch: 'full'
      },
      { path: 'members/:id', component: MemberDetailsWrapperComponent, pathMatch: 'full' },
      { path: 'member/edit', component: MemberEditComponent, resolve: { user: MemberEditResolver }, canDeactivate: [PreventUnsavedChangesGuard] },
      { path: 'massages', component: MessagesComponent, resolve: { msg: MessagesListResolver } },
      { path: 'list', component: ListsComponent, resolve: { members: ListResolver } }
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

