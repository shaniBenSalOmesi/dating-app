import { MessageActionTypes, MessageActions } from '../actions/messages.actions';
import { initialMessageState, MessageState } from '../state';

export function messageReducer(state = initialMessageState, action: MessageActions): MessageState {
  switch (action.type) {
    case MessageActionTypes.MESSAGE_LIST_SUCCSESS:
      return {
        messageList: action.payload,
      };
      case MessageActionTypes.MARK_AS_READ_SUCCSESS:
        return{
          messageList: action.payload
        };
      case MessageActionTypes.SEND_NEW_MESSAGE_SUCCSESS:
        return {
          messageList: action.payload
        };
    default:
      return state;
  }
}
