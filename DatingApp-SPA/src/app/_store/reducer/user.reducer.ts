import { UserActionTypes, UserActions } from '../actions/user.actions';
import { initialUserState, UserState, AppState } from '../state';

export function userReducer(state = initialUserState, action: UserActions): UserState {
  switch (action.type) {
    case UserActionTypes.LOGIN_COMPLETE:
      return {
        ...state,
        loggedIn: true,
        user: action.payload
      };
    case UserActionTypes.LOGOUT:
      return {
        ...state,
        loggedIn: false,
        user: null
      }
    default:
      return state;
  }
}
