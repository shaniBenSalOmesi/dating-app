import { MemberActionTypes, MemberActions } from '../actions/members.actions';
import { initialMemberState, MemberState } from '../state';

export function membersReducer(state = initialMemberState, action: MemberActions): MemberState {
  switch (action.type) {
    case MemberActionTypes.COMPLETE_MEMBER_LIST:
      return {
        ...state,
        memberList: (action.payload).result,
        memberPagination: (action.payload).pagination
      };
      case MemberActionTypes.GETֹֹֹֹ_SINGLE_MEMBER_SUCCSESS:
        return {
          memberList: state.memberList,
          memberPagination: state.memberPagination,
          selectedMember: action.payload
        };
    default:
      return state;
  }
}
