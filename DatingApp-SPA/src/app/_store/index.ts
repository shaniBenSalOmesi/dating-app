import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AppState, UserState, MemberState, MessageState } from './state';

export const selectUserState = (state: AppState) => state.userState;
export const selectMemberState =(state: AppState) => state.memberState;
export const selectMessageState = (state: AppState) => state.messageState;
export const selectCurrentUser = createSelector(
  selectUserState,
  (state: UserState) => {
    return (state.user);
  });
export const getLoggedIn = createSelector(
  selectUserState,
  (state: UserState) => state.loggedIn
);
export const getMembers = createSelector(
  selectMemberState,
  (state: MemberState)=> state.memberList
);
export const getPagination = createSelector(
  selectMemberState,
  (state: MemberState)=> state.memberPagination
);
export const getSelectedMember = createSelector(
  selectMemberState,
  (state: MemberState) => state.selectedMember
);
export const getMessageList =createSelector(
  selectMessageState,
  (state: MessageState) => state.messageList
);
