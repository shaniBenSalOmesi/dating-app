import { Action } from '@ngrx/store';
import { User } from 'src/app/_models/user';
import { UserForLogin } from 'src/app/_models/UserForLogin.model';
import { UserParams } from 'src/app/_models/UserParams.model';
import { PaginationResults } from 'src/app/_models/Pagination';

export enum MemberActionTypes {
  GET_MEMBERS = '[Members] GET_MEMBERS',
  COMPLETE_MEMBER_LIST = '[User] COMPLETE_MEMBER_LIST',
  UPDATEֹֹֹֹ_SINGLE_MEMBER ='[Member] UPDATEֹֹֹֹ_SINGLE_MEMBER',
  UPDATEֹֹֹֹ_SINGLE_MEMBER_SUCCSESS ='[Member] UPDATEֹֹֹֹ_SINGLE_MEMBER_SUCCSESS',
  GETֹֹֹֹ_SINGLE_MEMBER = '[Member] GETֹֹֹֹ_SINGLE_MEMBER',
  GETֹֹֹֹ_SINGLE_MEMBER_SUCCSESS = '[Member] GETֹֹֹֹ_SINGLE_MEMBER_SUCCSESS'
}

export class GetMembers implements Action {
  readonly type = MemberActionTypes.GET_MEMBERS;
  constructor(public payload: UserParams) { }
}
export class GetMembersComplete implements Action {
  readonly type = MemberActionTypes.COMPLETE_MEMBER_LIST;
  constructor(public payload: PaginationResults<User[]>) { }
}
export class UpdateSingleMember implements Action {
  readonly type = MemberActionTypes.UPDATEֹֹֹֹ_SINGLE_MEMBER;
  constructor(public payload){}
}
export class UpdateSingleMemberComplete implements Action {
  readonly type = MemberActionTypes.UPDATEֹֹֹֹ_SINGLE_MEMBER_SUCCSESS;
  constructor(public payload){}
}
export class GetSingleMemberDetails implements Action {
  readonly type = MemberActionTypes.GETֹֹֹֹ_SINGLE_MEMBER;
  constructor(public payload){}
}
export class GetSingleMemberDetailsComplete implements Action {
  readonly type = MemberActionTypes.GETֹֹֹֹ_SINGLE_MEMBER_SUCCSESS;
  constructor(public payload: User){}
}

export type MemberActions
  = GetMembers | GetMembersComplete | UpdateSingleMemberComplete | UpdateSingleMember | GetSingleMemberDetailsComplete;

