import { Action } from '@ngrx/store';
import { GetMessageListDto } from 'src/app/_models/dto/get-message-list.dto';
import { Message } from 'src/app/_models/Message';
import { MarkMsgReadDto } from 'src/app/_models/dto/mark-msg-read.dto';

export enum MessageActionTypes {
  GET_MESSAGES = '[Messages] GET_MESSAGES',
  MESSAGE_LIST_SUCCSESS = '[User] MESSAGE_LIST_SUCCSESS',
  SEND_NEW_MESSAGE ='[Message] SEND_NEW_MESSAGE',
  SEND_NEW_MESSAGE_SUCCSESS ='[Message] SEND_NEW_MESSAGE_SUCCSESS',
  MARK_AS_READ = '[Message] MARK_AS_READ',
  MARK_AS_READ_SUCCSESS = '[Message] MARK_AS_READ_SUCCSESS'
}

export class GetMessages implements Action {
  readonly type = MessageActionTypes.GET_MESSAGES;
  constructor(public payload: GetMessageListDto) { }
}
export class GetMessagesComplete implements Action {
  readonly type = MessageActionTypes.MESSAGE_LIST_SUCCSESS;
  constructor(public payload: Message[]) { }
}
export class SendMessage implements Action {
  readonly type = MessageActionTypes.SEND_NEW_MESSAGE;
  constructor(public payload){}
}
export class SendMessageComplete implements Action {
  readonly type = MessageActionTypes.SEND_NEW_MESSAGE_SUCCSESS;
  constructor(public payload){}
}

export class MarkMessageAsRead implements Action {
  readonly type = MessageActionTypes.MARK_AS_READ;
  constructor(public payload: MarkMsgReadDto){}
}

export class MarkMessageAsReadComplete implements Action {
  readonly type = MessageActionTypes.MARK_AS_READ_SUCCSESS;
  constructor(public payload){}
}

export type MessageActions
  = GetMessages | GetMessagesComplete | SendMessage | SendMessageComplete|
  MarkMessageAsRead | MarkMessageAsReadComplete ;

