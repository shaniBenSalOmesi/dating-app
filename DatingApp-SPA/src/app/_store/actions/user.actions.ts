import { Action } from '@ngrx/store';
import { User } from 'src/app/_models/user';
import { UserForLogin } from 'src/app/_models/UserForLogin.model';

export enum UserActionTypes {
  LOGIN = '[User] LOGIN',
  LOGIN_COMPLETE = '[User] LOGIN_COMPLETE',
  LOGIN_ERROR = '[User] LOGIN_ERROR',
  LOGOUT = '[User] LOGOUT',
  UPDATE_USER ='[User] UPDATE_USER'
}
export class Login implements Action {
  readonly type = UserActionTypes.LOGIN;
  constructor(public payload: UserForLogin) { }
}
export class LoginComplete implements Action {
  readonly type = UserActionTypes.LOGIN_COMPLETE;
  constructor(public payload: User) { }
}
export class Logout implements Action {
  readonly type = UserActionTypes.LOGOUT;
  constructor() { }
}
export type UserActions
  = Login | LoginComplete | Logout;

