import { User } from 'src/app/_models/user';
import { PaginationResults, Pagination } from 'src/app/_models/Pagination';
import { Message } from '../_models/Message';

export interface AppState {
  userState: UserState;
  memberState: MemberState;
  messageState: MessageState;
}

export interface UserState {
  user: User;
  loggedIn: boolean;
}

export interface MemberState {
  memberList: User[];
  memberPagination: Pagination;
  selectedMember: User;
}

export interface MessageState {
  messageList: Message[];
}

export const initialUserState: UserState = {
  user: null,
  loggedIn: false
}

export const initialMemberState: MemberState = {
  memberList: [],
  selectedMember: null,
  memberPagination: {
    currentPage: 1,
    itemsPerPage: null,
    totalPages: null,
    totalItems: null,
  }
}

export const initialMessageState: MessageState = {
  messageList: []
}
