import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';
import { UsersService } from 'src/app/_service/users.service';
import { GetMembers, MemberActionTypes, GetMembersComplete, GetSingleMemberDetails, GetSingleMemberDetailsComplete } from '../actions/members.actions';

@Injectable()
export class GetSelectedMemberEffect {
  constructor(private actions$: Actions, private memberService: UsersService) { }

  @Effect()
  tryGetMember$ = this.actions$.pipe(ofType<GetSingleMemberDetails>(MemberActionTypes.GETֹֹֹֹ_SINGLE_MEMBER),
    switchMap((action) => {
      return this.memberService.getUser(action.payload).pipe(
        map((member) => {
          return new GetSingleMemberDetailsComplete(member);
        }
        )
      )
    }))
}
