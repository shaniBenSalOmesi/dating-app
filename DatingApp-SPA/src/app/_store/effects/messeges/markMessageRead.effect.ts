import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, map, tap } from 'rxjs/operators';
import { UsersService } from 'src/app/_service/users.service';
import { GetMessages, MessageActionTypes, GetMessagesComplete, MarkMessageAsRead, MarkMessageAsReadComplete } from '../../actions/messages.actions';

@Injectable()
export class MarkAsMsgReadEffect {
  constructor(private actions$: Actions, private service: UsersService) { }

  @Effect()
  tryMarkMsgMessages$ = this.actions$.pipe(ofType<MarkMessageAsRead>(MessageActionTypes.MARK_AS_READ),
    map((action) => {
      return this.service.markMassageAsRead(action.payload.userId, action.payload.messageId);
    }))
}

