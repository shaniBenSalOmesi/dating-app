import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, map, tap } from 'rxjs/operators';
import { UsersService } from 'src/app/_service/users.service';
import { GetMessages, MessageActionTypes, GetMessagesComplete } from '../../actions/messages.actions';

@Injectable()
export class TryGetMessagesEffect {
  constructor(private actions$: Actions, private service: UsersService) { }

  @Effect()
  tryGetMessages$ = this.actions$.pipe(ofType<GetMessages>(MessageActionTypes.GET_MESSAGES),
    switchMap((action) => {
      return this.service.getMessageThred(action.payload).pipe(
        map((messagesList)=> {
          return new GetMessagesComplete(messagesList)
        })
      )
    }))
}

