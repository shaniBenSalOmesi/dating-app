import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, map } from 'rxjs/operators';
import { UsersService } from 'src/app/_service/users.service';
import { GetMembers, MemberActionTypes, GetMembersComplete } from '../actions/members.actions';

@Injectable()
export class TryGetMembersEffect {
  constructor(private actions$: Actions, private memberService: UsersService) { }

  @Effect()
  tryGetMembers$ = this.actions$.pipe(ofType<GetMembers>(MemberActionTypes.GET_MEMBERS),
    switchMap((action) => {
      return this.memberService.getUsers(action.payload).pipe(
        map((members) => new GetMembersComplete(members)
        )
      )
    }))
}

