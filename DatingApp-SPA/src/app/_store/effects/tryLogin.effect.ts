import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/_service/auth.service';
import { switchMap, map } from 'rxjs/operators';
import { UserActionTypes, LoginComplete, Login } from '../actions/user.actions';
import { Router } from '@angular/router';
import { AlertifyService } from 'src/app/_service/alertify.service';

@Injectable()
export class TryLoginEffect {
  constructor(private actions$: Actions, private authService: AuthService, ) { }

  @Effect()
  tryLogin$ = this.actions$.pipe(ofType<Login>(UserActionTypes.LOGIN),
    switchMap((action) => {
      return this.authService.login(action.payload).pipe(
        map((user) =>
          new LoginComplete(user)
        )
      )
    }))
}

