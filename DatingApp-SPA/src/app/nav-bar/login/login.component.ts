import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AlertifyService } from 'src/app/_service/alertify.service';
import { AuthService } from 'src/app/_service/auth.service';
import { Router } from '@angular/router';
import { UserState } from 'src/app/_store/state';
import { Store } from '@ngrx/store';
import { Login } from 'src/app/_store/actions/user.actions';
import { UserForLogin } from 'src/app/_models/UserForLogin.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: any = {};
  @Output() public loginEmmiter = new EventEmitter<UserForLogin>();

  constructor(
    public authService: AuthService,
    private router: Router,
    private notification: AlertifyService,
    private store: Store<UserState>
  ) { }

  ngOnInit() {
  }

  public login() {
    this.loginEmmiter.emit(this.user);
    // this.authService.login(this.user).subscribe(
    //   res => {
    //     this.notification.success('Login Succsess');
    //     this.router.navigate(['/members']);
    //   },
    //   err => this.notification.error(err)
    // );
  }

}
