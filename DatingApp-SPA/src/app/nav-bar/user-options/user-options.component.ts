import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { User } from 'src/app/_models/user';

@Component({
  selector: 'app-user-options',
  templateUrl: './user-options.component.html',
  styleUrls: ['./user-options.component.css']
})
export class UserOptionsComponent implements OnInit {

 @Input() public loggedUser: User;
  @Output() public onLogOut = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  public logOut(): void {
    this.onLogOut.emit();
  }
}
