import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { User } from '../_models/user';
import { UserForLogin } from '../_models/UserForLogin.model';
import { Store } from '@ngrx/store';
import { AppState } from '../_store/state';
import { AlertifyService } from '../_service/alertify.service';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { selectCurrentUser } from '../_store';
import { Logout, Login } from '../_store/actions/user.actions';
import General from '../_service/general.const';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavBarComponent implements OnInit, OnDestroy {
  public loggedUser: User;
  private unsubscribe$: Subject<void>;

  constructor(private store: Store<AppState>,
    private router: Router,
    private notification: AlertifyService,
    private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    this.unsubscribe$ = new Subject<void>();
    this.store
      .select(selectCurrentUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.loggedUser = res;
        this.cdr.detectChanges();
      });
   }

  public logOut(): void {
    localStorage.removeItem(General.token);
    localStorage.removeItem(General.user);
    this.store.dispatch(new Logout());
    this.notification.info('User Logged Out');
    this.router.navigate(['/home']);
  }
  public login(user: UserForLogin) {
    this.store.dispatch(new Login(user));
  }

  ngOnDestroy(){
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
