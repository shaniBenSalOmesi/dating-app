import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { UserState, AppState } from './_store/state';
import { Store, select } from '@ngrx/store';
import { from, Observable, Subject } from 'rxjs';
import { User } from './_models/user';
import { UserForLogin } from './_models/UserForLogin.model';
import { Login, Logout, LoginComplete } from './_store/actions/user.actions';
import General from './_service/general.const';
import { Router } from '@angular/router';
import { AlertifyService } from './_service/alertify.service';
import { tap, take, takeWhile, takeUntil } from 'rxjs/operators';
import { selectCurrentUser } from './_store';
import { AuthService } from './_service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  constructor(private store: Store<AppState>,
    private authService: AuthService,
    private router: Router,
    private cdr: ChangeDetectorRef) { }

  ngOnInit(): void {
    if(this.authService.loggedIn()){
      let user = this.authService.getUserFromLocalStorage();
      this.store.dispatch(new LoginComplete(user));
    }
  }

}
