import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../_service/auth.service';
import { AlertifyService } from '../_service/alertify.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, 
    private router: Router, 
    public notifications: AlertifyService){}
  canActivate(): boolean {
    if(this.authService.loggedIn()){
      return true;
    } else{
      this.notifications.error('You are not autorized for this route');
      this.router.navigate(['/home']);
      return false;
    }
  }
}
