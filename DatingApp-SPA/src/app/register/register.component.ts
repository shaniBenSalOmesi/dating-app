import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../_service/auth.service';
import { AlertifyService } from '../_service/alertify.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/bs-datepicker.config';
import { User } from '../_models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public user;
  public registerForm: FormGroup;
  public bsDateConfig: Partial<BsDatepickerConfig>;
  @Output() cancelRegister = new EventEmitter();

  constructor(public authService: AuthService, private notification: AlertifyService,
    public fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.generateForm();
    this.bsDateConfig = {
      containerClass: 'theme-red'
    }
    // this.registerForm = new FormGroup(
    //   {
    //     userName: new FormControl('', Validators.required),
    //     password: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]),
    //     confirmPassword: new FormControl('', Validators.required)
    //   }, this.valuesMatch
    // )
  }

  // generateForm FB
  public generateForm(): void {
    this.registerForm = this.fb.group({
      gender: ['male'],
      userName: ['', Validators.required],
      knownAs: ['', Validators.required],
      dateOfBirth: [null, Validators.required],
      city: [null, Validators.required],
      country: [null, Validators.required],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]],
      confirmPassword: ['', Validators.required]
    }, {validators: this.valuesMatch})
  }

  public register(): void {
    if(this.registerForm.valid){
      this.user = Object.assign({}, this.registerForm.value);
      this.authService.register(this.user).subscribe(() => {
        this.cancelRegister.emit();
        this.notification.success('Welcome');
      }, error => {this.notification.error('Register did not succssed')},
      ()=> {
        this.authService.login({userName: this.user.userName, password: this.user.password}).subscribe(res =>
          this.router.navigate(['/members']));
      });
    }
  }

  public cancel(): void {
    this.cancelRegister.emit();
  }

  public valuesMatch(form: FormGroup): any {
    return form.get('password').value === form.get('confirmPassword').value ? null : { misMatch: true }
  }

}
