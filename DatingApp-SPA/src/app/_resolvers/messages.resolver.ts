import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { UsersService } from '../_service/users.service';
import { User } from '../_models/user';
import { Route } from '@angular/compiler/src/core';
import { AlertifyService } from '../_service/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../_service/auth.service';
import { Message } from '../_models/Message';

@Injectable()
export class MessagesListResolver implements Resolve<Message[]> {

  pageNumber = 1;
  pageSize = 5
  messageContainer = 'Unread';
  constructor(private userService: UsersService,
    private router: Router,
    private notification: AlertifyService, private authService: AuthService) { }

  resolve(router: ActivatedRouteSnapshot): Observable<Message[]> {
    return this.userService.getMessages(this.authService.decodeToken.nameid, this.pageSize, this.pageNumber, this.messageContainer)
    .pipe(
      catchError(err => {
        this.notification.error(err);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }
}
