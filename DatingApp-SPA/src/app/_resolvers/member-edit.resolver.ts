import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { User } from '../_models/user';
import { UsersService } from '../_service/users.service';
import { AlertifyService } from '../_service/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../_service/auth.service';

@Injectable()

export class MemberEditResolver implements Resolve<User> {
  constructor(private service: UsersService, private authService: AuthService, private router: Router, private notifications: AlertifyService){}
  resolve(route: ActivatedRouteSnapshot): Observable<User>{
    let loggedUserId: number = this.authService.decodeToken.nameid;

    return this.service.getUser(loggedUserId).pipe(catchError(err => {
      this.notifications.error('problem retriving your data');
      this.router.navigate(['/members']);
      return of (null);
    }))
  }
}
