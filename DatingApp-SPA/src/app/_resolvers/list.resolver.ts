import { Injectable } from "@angular/core";
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { UsersService } from '../_service/users.service';
import { User } from '../_models/user';
import { Route } from '@angular/compiler/src/core';
import { AlertifyService } from '../_service/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ListResolver implements Resolve<User[]> {

  params = {
    pageNumber: 1,
    pageSize: 5
  }
  likeParams = 'likers';
  constructor(private userService: UsersService, private router: Router, private notification: AlertifyService ){}

  resolve(router: ActivatedRouteSnapshot): Observable<User[]> {

    return this.userService.getUsers(this.params, this.likeParams).pipe(
      catchError(err => {
        this.notification.error(err);
        this.router.navigate(['/home']);
        return of(null);
      })
    )
  }
}
