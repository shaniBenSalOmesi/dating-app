import { Component, OnInit } from '@angular/core';
import { Message } from '../_models/Message';
import { Pagination, PaginationResults } from '../_models/Pagination';
import { AuthService } from '../_service/auth.service';
import { UsersService } from '../_service/users.service';
import { ActivatedRoute } from '@angular/router';
import { AlertifyService } from '../_service/alertify.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  public messages: Message[]=[];
  public pagination: Pagination = {
    currentPage: null,
    itemsPerPage: null,
    totalItems: null,
    totalPages: null
  }
  public messageContainer = 'Unread';
  constructor(private authService: AuthService, private userService: UsersService, private notification: AlertifyService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.messages = data['msg'].result;
      this.pagination = data['msg'].pagination;
    })
    this.messageContainer = 'Unread';
  }

  public loadMesseges(): any {
    this.userService.getMessages(this.authService.decodeToken.nameid,
      this.pagination.itemsPerPage, this.pagination.currentPage, this.messageContainer).subscribe(
      (res: PaginationResults<Message[]>) => {
        this.messages = res.result;
        this.pagination = res.pagination;
      }, error => this.notification.error(error));
  }

  public pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadMesseges();
  }

  public removeMessageConfirmation(msgId: number):void {
    this.notification.confirm('Are You Sure You Want To Delete This Message?', () => this.removeMessage(msgId));
  }

  private removeMessage(msgId: number): any {
    this.userService.removeMessageForUser(this.authService.decodeToken.nameid, msgId).subscribe(()=> {
      this.messages = this.messages.filter(msg => msg.id != msgId);
      this.notification.success('The message was deleted succsesfully!');
    }, err => this.notification.error(err));

  }

}
