import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import General from './general.const';
import { User } from '../_models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = environment.apiUrl + 'auth/';
  private jwtHelper = new JwtHelperService();
  public decodeToken: any;
  constructor(private http: HttpClient) { }

  public login(user) {
    return this.http.post(this.baseUrl + 'login', user).pipe(map((response: any) => {
      const userToken = response;
      if (userToken) {
        localStorage.setItem(General.token, userToken.token);
        localStorage.setItem(General.user, JSON.stringify(userToken.userToReturn));
        this.decodeToken = this.jwtHelper.decodeToken(userToken.token);
        return userToken.userToReturn;
      }
    }));
  }

  public loggedIn(): boolean {
    const token = localStorage.getItem(General.token);
    let isTokenValid: boolean = !this.jwtHelper.isTokenExpired(token);
    if (token && isTokenValid) {
      return isTokenValid;
    } else localStorage.removeItem(General.token);
  }

  public

  public getUserFromLocalStorage(): User {
    let user = localStorage.getItem(General.user);
    if(!this.decodeToken){
      let token = localStorage.getItem(General.token);
      this.decodeToken = this.jwtHelper.decodeToken(token);
    }
    return JSON.parse(user);
  }

  public register(user: User) {
    return this.http.post(this.baseUrl + 'register', user);
  }
}
