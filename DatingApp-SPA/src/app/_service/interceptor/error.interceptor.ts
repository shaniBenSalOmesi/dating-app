import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    intercept(
      req: import('@angular/common/http').HttpRequest<any>,
      next: import('@angular/common/http').HttpHandler
    ): import('rxjs').Observable<import('@angular/common/http').HttpEvent<any>> {
      return next.handle(req).pipe(
          catchError(error => {
              if (error.status === 401) {
                  return throwError(error.statusText);
              }
              if (error instanceof HttpErrorResponse) {
                  //errors from server 500+
                  const applicationError = error.headers.get('Application-Error');
                  if (applicationError) {
                      return throwError(applicationError);
                  }
                  //we devide for 2 situations: if its 1 err or if its couple of errors
                  const serverError = error.error; //if only 1 err its just error.error;
                  let modalstateErrors = '';
                  if (serverError.errors && typeof serverError.errors === 'object') {
                      for (const key in serverError.errors) {
                          if (serverError.errors[key]) {
                              modalstateErrors += serverError.errors[key] + '\n'; //if its more then 1 error will contain errors obj
                              //the error obj will contain key and value as string or array of strings
                          }
                      }
                  }
                  return throwError(modalstateErrors || serverError || 'Server Error');
              }
          })
      );
    }
  }
  
  
  
  export const ErrorInterceptorProvider = {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
  };