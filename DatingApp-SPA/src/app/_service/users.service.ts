import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { Pagination, PaginationResults } from '../_models/Pagination';
import { map } from 'rxjs/operators';
import { Message } from '../_models/Message';
import { MessageDto } from '../_models/dto/MessageDto';
import { UserParams } from '../_models/UserParams.model';
import { GetMessageListDto } from '../_models/dto/get-message-list.dto';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  private baseUrl: string = environment.apiUrl + 'users';

  constructor(private http: HttpClient) { }

  //Lesson 143
  public getUsers(paramsFromUser: UserParams, likeParams?): Observable<PaginationResults<User[]>> {
    const paginationRes: PaginationResults<User[]> = new PaginationResults<User[]>();
    let params = new HttpParams();

    if (paramsFromUser.currentPage != null) {
      params = params.append('pageNumber', paramsFromUser.currentPage.toString());
    }

    if (paramsFromUser.itemsPerPage != null) {
      params = params.append('pageSize', paramsFromUser.itemsPerPage.toString());
    }

    if (paramsFromUser.extraParams != null) {
      params = params.append('minAge', paramsFromUser.extraParams.minAge);
      params = params.append('maxAge', paramsFromUser.extraParams.maxAge);
      params = params.append('gender', paramsFromUser.extraParams.gender);
      params = params.append('orderBy', paramsFromUser.extraParams.orderBy);
    }

    if (likeParams == 'likers') {
      params = params.append('likers', 'true');
    }

    if (likeParams == 'likees') {
      params = params.append('likees', 'true');
    }

    return this.http.get<User[]>(this.baseUrl, { observe: 'response', params }).pipe(map(response => {
      paginationRes.result = response.body;
      if (response.headers.get('Pagination') != null) {
        paginationRes.pagination = JSON.parse(response.headers.get('Pagination'))
      };
      return paginationRes;
    }));
  }

  //before adding pagination
  // public getUsers(): Observable<User[]> {
  //   return this.http.get<User[]>(this.baseUrl);
  // }

  public getUser(userId: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + `/${userId}`);
  }

  public updateUser(userId: number, updatedUser): Observable<void> {
    return this.http.put<void>(this.baseUrl + '/' + userId, updatedUser);
  }

  public setMainPhoto(userId: number, id: number): Observable<void> {
    return this.http.post<void>(this.baseUrl + '/' + userId + '/photos/' + id + '/setMain', {});
  }

  public deletePhoto(userId: number, id: number): Observable<void> {
    return this.http.delete<void>(this.baseUrl + '/' + userId + '/photos/' + id);
  }

  public sendLike(id: number, resepientId: number): Observable<any> {
    return this.http.post(this.baseUrl + '/' + id + '/like/' + resepientId, {});
  }

  public getMessages(userId: number, pageSize?, pageNumber?, messageContainer?): Observable<PaginationResults<Message[]>> {
    const paginationRes: PaginationResults<Message[]> = new PaginationResults<Message[]>();
    let params = new HttpParams();
    params = params.append('MessageContainer', messageContainer);
    if (pageSize !== null) {
      params = params.append('pageSize', pageSize);
    }
    if (pageNumber !== null) {
      params = params.append('pageNumber', pageNumber);
    }
      return this.http.get<Message[]>(this.baseUrl + '/' + userId + '/message', {observe: 'response', params}).pipe(map (response => {
        paginationRes.result = response.body;
        if (response.headers.get('Pagination') != null) {
          paginationRes.pagination = JSON.parse(response.headers.get('Pagination'))
        };
        return paginationRes;
      }));
    }

    public getMessageThred(params: GetMessageListDto): Observable<Message[]>{
      return this.http.get<Message[]>(this.baseUrl + '/' + params.userId + '/message/thread/' + params.recipientId);
    }

    public sendNewMessage(userId: number, message: MessageDto): Observable<any> {
      return this.http.post<Message>(this.baseUrl + '/' + userId + '/message', message);
    }

    public removeMessageForUser(userId: number, messageId: number) {
      return this.http.post(this.baseUrl + '/' + userId + '/message/' + messageId, {});
    }

    public markMassageAsRead(userId: number, messageId: number){
      return this.http.post(this.baseUrl + '/' + userId + '/message/' + messageId + '/read', {}).subscribe();
    }
  }
