const General = {
  token: 'datingToken',
  user: 'datingUser',
  userDefaultImageSrc: '../../assets/images/default-user-image.png'
};

export default General;
