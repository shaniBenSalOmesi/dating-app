export interface Pagination {
  currentPage: number;
  itemsPerPage: number;
  totalPages: number;
  totalItems: number;
}

export class PaginationResults<T> {
  result: T;
  pagination: Pagination;
}
