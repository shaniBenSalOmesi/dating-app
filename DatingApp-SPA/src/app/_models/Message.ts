export interface Message {
  id: number;
  content: string;
  senderPhotoUrl: string;
  senderId: number;
  senderKnownAs: string;
  recipientPhotoUrl: string;
  recipientId: number;
  recipientKnownAs: string;
  isRead: boolean;
  messageSent: boolean;
  dateRead: Date;
}
