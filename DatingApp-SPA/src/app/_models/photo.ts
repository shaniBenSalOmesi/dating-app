export interface Photo {
  id: string;
  url: string;
  isMain: boolean;
  description: string;
  dateAdded: Date;
}
