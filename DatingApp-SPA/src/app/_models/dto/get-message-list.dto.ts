export interface GetMessageListDto {
  userId: number;
  recipientId: number;
}
