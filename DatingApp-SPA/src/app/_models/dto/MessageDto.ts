export interface MessageDto {
  recipientId: number;
  content: string;
}
