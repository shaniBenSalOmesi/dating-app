export interface MarkMsgReadDto {
  userId: number;
  messageId: number;
}
