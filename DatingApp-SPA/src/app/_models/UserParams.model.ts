export interface UserParams {
  pageNumber?: number,
  pageSize?: number,
  currentPage?: number,
  itemsPerPage?: number,
  extraParams?: any
}
