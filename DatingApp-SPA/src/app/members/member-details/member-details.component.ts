import { Component, OnInit, ViewChild, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { User } from 'src/app/_models/user';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from '@kolkov/ngx-gallery';
import General from 'src/app/_service/general.const';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { getSelectedMember, getMessageList } from 'src/app/_store';
import { AppState } from 'src/app/_store/state';
import { Store } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Message } from 'src/app/_models/Message';
import { UsersService } from 'src/app/_service/users.service';
import { AuthService } from 'src/app/_service/auth.service';
import { GetMessageListDto } from 'src/app/_models/dto/get-message-list.dto';
import { GetMessages } from 'src/app/_store/actions/messages.actions';

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnDestroy {
  public member: User;
  public galleryOptions: NgxGalleryOptions[];
  public galleryImages: NgxGalleryImage[];
  public general = General;
  public messeges: Message[];
  private unsubscribe$: Subject<void>;
  private loggedUserId: number;
  @ViewChild('staticTabs', { static: true }) staticTabs: TabsetComponent;

  constructor(private router: ActivatedRoute,
    private userService: UsersService,
    private authService: AuthService,
    private store: Store<AppState>,
    private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.loggedUserId = +this.authService.decodeToken.nameid;
    this.unsubscribe$ = new Subject<void>();
    this.store.select(getSelectedMember).pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => {
      this.member = res;
      if (!!res){
        this.loadMessages();
      }
      this.cdr.detectChanges();
      if(!!res && !!res.photos.length){
          this.galleryImages = this.getImages();
        }
      });
    this.galleryOptions = [
      {
        width: '500px', height: '500px',
        thumbnailsColumns: 4, imagePercent: 100,
        imageAnimation: NgxGalleryAnimation.Slide, preview: false
      }
    ];
    this.router.queryParams.subscribe(params => {
      if (params['tabNumber'] != null) {
        this.openTab(params['tabNumber']);
      }
    });
    this.store.select(getMessageList)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.messeges = res;
        if(res.length > 0){
          res.forEach(msg => {
            if (!msg.isRead && msg.recipientId == this.loggedUserId)
              this.userService.markMassageAsRead(this.loggedUserId, msg.id);
          })
        }
        this.cdr.detectChanges();
      });
  }

  public openTab(tabId: number): void {
    this.staticTabs.tabs[tabId].active = true;
  }

  private getImages() {
    const imageUrl = [];
    for (const image of this.member.photos) {
      imageUrl.push({
        small: image.url,
        medium: image.url,
        big: image.url,
        description: image.description
      });
    }
    return imageUrl;
  }

  public loadMessages(): void {
    let params: GetMessageListDto = {
      userId: this.loggedUserId,
      recipientId: this.member.id
    }
    this.store.dispatch(new GetMessages(params));
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
