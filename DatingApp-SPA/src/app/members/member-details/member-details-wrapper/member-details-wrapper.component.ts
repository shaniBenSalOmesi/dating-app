import { Component, OnInit } from '@angular/core';
import { GetSingleMemberDetails } from 'src/app/_store/actions/members.actions';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { User } from 'src/app/_models/user';
import { ActivatedRoute } from '@angular/router';
import { AppState } from 'src/app/_store/state';

@Component({
  selector: 'app-member-details-wrapper',
  templateUrl: './member-details-wrapper.component.html',
  styleUrls: ['./member-details-wrapper.component.css']
})
export class MemberDetailsWrapperComponent implements OnInit {
  private userId: number;
  public member: User;

  constructor(private router: ActivatedRoute, private store: Store<AppState>) { }

  ngOnInit() {
    this.userId = this.router.snapshot.params['id'];
    this.store.dispatch(new GetSingleMemberDetails(this.userId));
  }
}
