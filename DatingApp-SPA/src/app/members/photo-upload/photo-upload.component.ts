import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Photo } from 'src/app/_models/photo';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/_service/auth.service';
import General from 'src/app/_service/general.const';
import { UsersService } from 'src/app/_service/users.service';
import { AlertifyService } from 'src/app/_service/alertify.service';

@Component({
  selector: 'app-photo-upload',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.css']
})
export class PhotoUploadComponent implements OnInit {
  @Input() public photos: Photo[] = [];
  @Output() public updateMainImageEmmiter = new EventEmitter<string>();
  uploader: FileUploader;
  hasBaseDropZoneOver: boolean;
  response: string;
  private baseUrl = environment.apiUrl + 'users/';
  private userId: number;

  constructor(private authService: AuthService, private userService: UsersService, private notifications: AlertifyService) { }

  ngOnInit(): void {
    this.userId = this.authService.decodeToken.nameid;
    this.initializeUploader();
  }

  initializeUploader() {
    this.uploader = new FileUploader({
      url:
        this.baseUrl +
        this.authService.decodeToken.nameid +
        '/photos', // we need the user id
      authToken: 'Bearer ' + localStorage.getItem(General.token), // we need to pass the token
      isHTML5: true,
      allowedMimeType: ['image/jpeg', 'images', 'png', 'jpg'],
      maxFileSize: 10 * 1024 * 1024, // it will make the maximum file size of 10mbs
      autoUpload: false, // click a button in order to send this up
      removeAfterUpload: true // after the photo is being uploaded we want to remove it from the upload queue
    });

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; }

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        const res: Photo = JSON.parse(response);
        let photo = {
          id: res.id,
          url: res.url,
          isMain: res.isMain,
          dateAdded: res.dateAdded,
          description: res.description
        };
        this.photos.push(photo);
        if(photo.isMain) {
          this.updateMainImageEmmiter.emit(photo.url);
        }
      }
    };
  }

  public setMainPhoto(photoId: number) {
    this.userService.setMainPhoto(this.userId, photoId).subscribe(() => {
      let currentMainIdx: number = this.photos.findIndex(p => !!p.isMain);
      this.photos[currentMainIdx].isMain = false;
      let photoIdx: number = this.photos.findIndex(p => +p.id == photoId);
      this.photos[photoIdx].isMain = true;
      this.updateMainImageEmmiter.emit(this.photos[photoIdx].url);
     }
      , error => this.notifications.error(error));
  }

  public deletePhoto(photoId: number): void {
    let photoIdx: number = this.photos.findIndex(p => +p.id == photoId);
    if (this.photos[photoIdx].isMain){
      this.notifications.error('You cannot delete your main photo!');
    } else {
      this.notifications.confirm('Are you sure you want to delete this photo?', () => this.deleteSelectedPhoto(photoId));
    }
  }

  private deleteSelectedPhoto(photoId: number): any {
    this.userService.deletePhoto(this.userId, photoId).subscribe((res) => {
      this.photos = this.photos.filter(photo => +photo.id !== photoId);
      this.notifications.success('Photo deleted!');
    },
    (err) => this.notifications.error(err));
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

}
