import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/_models/user';
import { AlertifyService } from 'src/app/_service/alertify.service';
import { NgForm } from '@angular/forms';
import { UsersService } from 'src/app/_service/users.service';
import { AuthService } from 'src/app/_service/auth.service';
import General from 'src/app/_service/general.const';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  public user: User;
  @ViewChild('formToEdit') public formToEdit: NgForm;
  constructor(private router: ActivatedRoute,
    private readonly notification: AlertifyService,
    private service: UsersService, private authService: AuthService) { }

  ngOnInit() {
    this.router.data.subscribe(data => this.user = data['user']);
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.formToEdit.dirty) {
      $event.returnValue = true;
    }
  }

  public saveUpdatedUser() {
    let userTokenId: number = this.authService.decodeToken.nameid;
    this.service.updateUser(userTokenId, this.user).subscribe(()=> {
      this.formToEdit.reset(this.user);
      this.notification.success('Profile Updated Succsesfully');
    }, error => this.notification.error(error));

  }

  public updateMainPhoto(selectedPhoto: string){
    this.user.photoUrl = selectedPhoto;
    localStorage.setItem(General.user, JSON.stringify(this.user));
  }
}
