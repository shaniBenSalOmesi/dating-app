import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/_models/user';
import General from 'src/app/_service/general.const';

@Component({
  selector: 'app-memder-details-left',
  templateUrl: './memder-details-left.component.html',
  styleUrls: ['./memder-details-left.component.css']
})
export class MemderDetailsLeftComponent implements OnInit {
  @Input() public member: User;
  public general = General;
  constructor() { }

  ngOnInit() {
  }

}
