import { Component, OnInit, Output, Input, EventEmitter, ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';
import { User } from 'src/app/_models/user';
import { Pagination } from 'src/app/_models/Pagination';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})
export class MemberListComponent implements OnChanges {
  @Input() members: User[] = [];
  @Input() pagination: Pagination;
  public paginationCopy: Pagination;
  @Output() public onPageChange = new EventEmitter<number>();
  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    this.paginationCopy = Object.assign({}, this.pagination);
  }
  public onPageChanged(event: any): void{
    this.onPageChange.emit(event.page);
  }
}
