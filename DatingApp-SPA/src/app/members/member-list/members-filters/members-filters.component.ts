import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { User } from 'src/app/_models/user';
import General from 'src/app/_service/general.const';

@Component({
  selector: 'app-members-filters',
  templateUrl: './members-filters.component.html',
  styleUrls: ['./members-filters.component.css']
})
export class MembersFiltersComponent implements OnChanges {
  @Input() public userParams: any = {};
  public params: any = {};
  public genderList: {label: string, value}[] = [{label: 'Male', value: 'male'}, {label: 'Female', value: 'female'}];
  @Output() public onFilterUpdate = new EventEmitter<any>();
  @Output() public onFilterReset = new EventEmitter<void>();
  constructor() { }
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.params = Object.assign({}, this.userParams);
  }

  public updateFilter(): void {
    this.onFilterUpdate.emit(this.params);
  }

  public resetFilter(): void {
    this.onFilterReset.emit();
  }

}
