import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/_models/user';
import General from 'src/app/_service/general.const';
import { UsersService } from 'src/app/_service/users.service';
import { AlertifyService } from 'src/app/_service/alertify.service';
import { AuthService } from 'src/app/_service/auth.service';

@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.css']
})
export class MemberCardComponent implements OnInit {
  @Input() member: User;
  public general = General;
  constructor(private service: UsersService, private notification: AlertifyService, private authService: AuthService) { }

  ngOnInit() {
  }
  public likeMember(){
    let userId = this.authService.decodeToken.nameid;
    this.service.sendLike(userId, this.member.id).subscribe(res=> {
      this.notification.success('You like '+ this.member.knownAs);
    }, err=> this.notification.error(err));

  }
}
