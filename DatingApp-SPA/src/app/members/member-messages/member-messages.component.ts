import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { Message } from 'src/app/_models/Message';
import { UsersService } from 'src/app/_service/users.service';
import { AuthService } from 'src/app/_service/auth.service';
import { AlertifyService } from 'src/app/_service/alertify.service';
import { MessageDto } from 'src/app/_models/dto/MessageDto';
import { tap, takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/_store/state';
import { GetMessageListDto } from 'src/app/_models/dto/get-message-list.dto';
import { GetMessages } from 'src/app/_store/actions/messages.actions';
import { Subject } from 'rxjs';
import { getMessageList } from 'src/app/_store';

@Component({
  selector: 'app-member-messages',
  templateUrl: './member-messages.component.html',
  styleUrls: ['./member-messages.component.css']
})
export class MemberMessagesComponent implements OnInit, OnDestroy {
  @Input() public recipientId: number;
  public userId: number;
  @Input() public messages: Message[] = [];
  public msgContent: string = '';
  private unsubscribe$: Subject<void>;
  @Output() public loadMsgsEmmiter= new EventEmitter<void>();

  constructor(
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
    public userService: UsersService,
    private authService: AuthService,
    private notifications: AlertifyService) { }


  ngOnInit() {
    this.userId = +this.authService.decodeToken.nameid;
    this.unsubscribe$ = new Subject<void>();
  }

  public sendMessage(): void {
    let message: MessageDto = {
      content: this.msgContent,
      recipientId: this.recipientId
    }
    this.userService.sendNewMessage(this.userId, message).subscribe(res => {
      this.msgContent = '';
      // this.messages.unshift(res)
      this.loadMsgsEmmiter.emit()
    },
      err => this.notifications.error(err));
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
// this.userService.getMessageThred(params)
//     .pipe(tap(res => {
//       res.forEach(msg => {
//         if(!msg.isRead && msg.recipientId == this.userId)
//         this.userService.markMassageAsRead(this.userId, msg.id);
//       })
//     }))
//       .subscribe(res => {
//         this.messages = res },
//         err => this.notifications.error(err));
