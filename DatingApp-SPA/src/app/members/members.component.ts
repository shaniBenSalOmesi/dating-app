import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { User } from '../_models/user';
import { Pagination } from '../_models/Pagination';
import { UsersService } from '../_service/users.service';
import { AlertifyService } from '../_service/alertify.service';
import General from '../_service/general.const';
import { AppState } from '../_store/state';
import { Store } from '@ngrx/store';
import { GetMembers } from '../_store/actions/members.actions';
import { Subject } from 'rxjs';
import { takeUntil, take, takeWhile } from 'rxjs/operators';
import { selectMemberState, getPagination, getMembers } from '../_store';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit, OnDestroy {
  public members: User[] = [];
  public pagination: Pagination = {
    currentPage: null,
    itemsPerPage: null,
    totalItems: null,
    totalPages: null
  };

  public user: User = JSON.parse(localStorage.getItem(General.user));
  public userParams: any = {};
  private unsubscribe$: Subject<void>;

  constructor(private service: UsersService,
    private notifications: AlertifyService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>) {
  }

  ngOnInit() {
    this.resetFilters();
    this.unsubscribe$ = new Subject<void>();
    this.store.select(getPagination)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(res => this.pagination = res);
    this.store
      .select(getMembers)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(res => {
        this.members = res;
        this.cdr.detectChanges();
      });
    }

    public updateFilter(filters): void {
      this.userParams = filters;
      this.loadUsers();
    }

    public resetFilters(): void {
      let gender = this.user.gender == 'female' ? 'male' : 'female';
      this.userParams = {
        gender,
        minAge: 18,
        maxAge: 99,
        orderBy: 'lastActive'
      }
      this.loadUsers();
    }

    public loadUsers(): any {
      let params = {
        currentPage: this.pagination.currentPage,
        itemsPerPage: this.pagination.itemsPerPage,
        extraParams: this.userParams
      }
      this.store.dispatch(new GetMembers(params));
    // this.service.getUsers(params).subscribe(
    //   (res: PaginationResults<User[]>) => {
    //     this.members = res.result;
    //     this.pagination = res.pagination;
    //   }, error => this.notifications.error(error));
  }

  public pageChanged(event: any): void {
    this.pagination.currentPage = event;
    this.loadUsers();
  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
