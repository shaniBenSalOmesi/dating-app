import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { Pagination, PaginationResults } from '../_models/Pagination';
import { AuthService } from '../_service/auth.service';
import { UsersService } from '../_service/users.service';
import { AlertifyService } from '../_service/alertify.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {
  public members: User[] = [];
  public pagination: Pagination;
  public likesParams: string;
  constructor(private authService: AuthService, private userService: UsersService, private notification: AlertifyService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.members = data['members'].results;
      this.pagination = data['members'].pagination;
    })
    this.likesParams = 'likers';
  }

  public loadUsers(): any {
    let params = {
      currentPage: this.pagination.currentPage,
      itemsPerPage: this.pagination.itemsPerPage,
    }
    this.userService.getUsers(params, this.likesParams).subscribe(
      (res: PaginationResults<User[]>) => {
        this.members = res.result;
        this.pagination = res.pagination;
      }, error => this.notification.error(error));
  }

  public pageChanged(event: any): void {
    this.pagination.currentPage = event;
    this.loadUsers();
  }
}
